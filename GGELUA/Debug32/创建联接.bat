@echo off
mkdir lib
mklink /j .\lua ..\Release64\lua
mklink /j .\test ..\Release64\test
mklink /j .\tools ..\Release64\tools

mklink /h  .\build.lua ..\Release64\build.lua
mklink /h  .\ggelua.lua ..\Release64\ggelua.lua
mklink /h  .\main.lua ..\Release64\main.lua
pause