--[[
    @Author       : GGELUA
    @Date         : 2021-05-08 01:11:14
    @LastEditTime : 2021-05-08 01:49:43
--]]
local im = require"gimgui"
local IMBase = require"IMGUI.基类"

local IM纹理 = class('IM纹理',"IMBase")

function IM纹理:初始化(t)
    self._tex = t
end

function IM纹理:显示(x,y)
    local ptr = self._tex:取对象():GetTexturePointer()
    im.Image(ptr)
end

return IM纹理