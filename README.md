
GGELUA
------
GGELUA是一个基于[SDL](https://github.com/libsdl-org/SDL)简单的2D游戏引擎，用纯lua代码编写游戏，类似[love2d](https://github.com/love2d/love),EM，但比love2d更简单，比EM更完善

[官方网站](http://bbs.ggelua.com/)

[![QQ群](https://img.shields.io/badge/QQ群-925879733-brightgreen)](https://qm.qq.com/cgi-bin/qm/qr?k=jX-XZqJT-1f9PTj0UfBz5XBKcRnHp5Ex&jump_from=webapi)

目标:
----
- 支持 [bgfx](https://github.com/cloudwu/lua-bgfx)
- 支持 ![进度](https://img.shields.io/badge/安卓-90%25-green) ![进度](https://img.shields.io/badge/WINDOWS-90%25-green)
- 整合 [VSCODE](https://code.visualstudio.com/)(调试,跳转,提示,文档,支持库等)
- $\color{#FF3030}{支持}$ [TypeScriptToLua](https://github.com/TypeScriptToLua/TypeScriptToLua)
- $\color{#FF3030}{支持}$ [teal](https://github.com/teal-language/tl)

[100%]:https://img.shields.io/badge/进度-100%25-brightgreen
[70%]:https://img.shields.io/badge/进度-70%25-yellow
[0%]:https://img.shields.io/badge/进度-0%25-red
[50%]:https://img.shields.io/badge/进度-50%25-yellow
完成进度:
------
|功能|进度|
|-|-|
|class| ![100%]
|动画类| ![100%]
|矩形(包围盒)| ![100%]
|文本类| ![100%]
|资源类| ![100%]
|坐标类| ![100%]
|引擎[SDL](https://github.com/libsdl-org/SDL)| ![进度](https://img.shields.io/badge/进度-60%25-yellow)
|界面GUI| ![进度](https://img.shields.io/badge/进度-90%25-green)
|网络[HPSOCKET](https://gitee.com/ldcsaa/HP-Socket)| ![进度](https://img.shields.io/badge/进度-90%25-green)
|寻路AStar| ![100%]
|视频[FFMPEG](https://github.com/ShiftMediaProject/FFmpeg)| ![进度](https://img.shields.io/badge/进度-90%25-green)
|工具[IMGUI](https://github.com/ocornut/imgui)| ![进度](https://img.shields.io/badge/进度-80%25-yellowgreen)
[IP地址](https://gitee.com/lionsoul/ip2region)|![100%]
|[TILED](https://github.com/mapeditor/tiled)| ![0%]

引擎:
----
|功能|进度|
|-|-|
|线程| ![0%]
|手柄| ![50%]
|[HGE](https://github.com/kvakvs/hge)粒子| ![0%]
|IME| ![0%]
|录音| ![50%]
|VSCODE| ![进度](https://img.shields.io/badge/进度-10%25-orange)
|服务端| ![进度](https://img.shields.io/badge/进度-10%25-orange)
|实例| ![0%]
|[bgfx](https://github.com/cloudwu/lua-bgfx)| ![0%]

[_luasql]:http://keplerproject.github.io/luasql/
[_lfs]:http://keplerproject.github.io/luafilesystem/
[_md5]:http://keplerproject.github.com/md5
[_base64]:https://web.tecgraf.puc-rio.br/~lhf/ftp/lua/#lbase64
[_zlib]:https://github.com/brimworks/lua-zlib
[_cjson]:https://github.com/openresty/lua-cjson
[_mongo]:https://github.com/neoxic/lua-mongo
[_lsqlite3]:http://lua.sqlite.org/
[_socket]:https://github.com/diegonehab/luasocket
[_cmsgpack]:https://github.com/antirez/lua-cmsgpack
[_zmq]:https://github.com/zeromq/lzmq
[_cffi]:https://github.com/q66/cffi-lua
库:
---
|功能|进度|功能|进度|
|-|-|-|-|
|[luasql.mysql][_luasql]|![100%]|[luasql.sqlite3][_luasql]|![100%]
|[luasql.postgres][_luasql]|![100%]|[luasql.odbc][_luasql]|![100%]
|[lfs][_lfs]|![100%]|[md5][_md5]|![100%]
|[base64][_base64]|![100%]|[zlib][_zlib]|![100%]
|[cjson][_cjson]|![100%]|[mongo][_mongo]|![100%]
|[lsqlite3][_lsqlite3]|![100%]|[socket][_socket]|![100%]
|[cmsgpack][_cmsgpack]|![100%]|[zmq][_zmq]|![100%]
|[cffi][_cffi]|![0%]
|redis|![0%]
|[rapidjson](https://github.com/xpol/lua-rapidjson)|![0%]
|[RmlUi](https://github.com/mikke89/RmlUi)|![0%]

编译

```
git submodule init
git submodule update
```

![进度](赞赏.png)