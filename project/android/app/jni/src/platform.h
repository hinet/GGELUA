﻿#ifndef _PLATFORMCONFIG_H

#define GGE_PLATFORM_UNKNOWN            0
#define GGE_PLATFORM_IOS                1
#define GGE_PLATFORM_ANDROID            2
#define GGE_PLATFORM_WIN32              3
#define GGE_PLATFORM_LINUX              4
#define GGE_PLATFORM_MAC                5

// Determine target platform by compile environment macro.
#define GGE_TARGET_PLATFORM             GGE_PLATFORM_UNKNOWN

// Apple: Mac and iOS
#if defined(__APPLE__) && !defined(ANDROID) // exclude android for binding generator.
#include <TargetConditionals.h>
#if TARGET_OS_IPHONE // TARGET_OS_IPHONE includes TARGET_OS_IOS TARGET_OS_TV and TARGET_OS_WATCH. see TargetConditionals.h
#undef  GGE_TARGET_PLATFORM
#define GGE_TARGET_PLATFORM         GGE_PLATFORM_IOS
#elif TARGET_OS_MAC
#undef  GGE_TARGET_PLATFORM
#define GGE_TARGET_PLATFORM         GGE_PLATFORM_MAC
#endif
#endif

// android
#if defined(ANDROID)
#undef  GGE_TARGET_PLATFORM
#define GGE_TARGET_PLATFORM         GGE_PLATFORM_ANDROID
#endif

// win32
#if defined(_WIN32) && defined(_WINDOWS)
#undef  GGE_TARGET_PLATFORM
#define GGE_TARGET_PLATFORM         GGE_PLATFORM_WIN32
#endif

// linux
#if defined(LINUX) && !defined(__APPLE__)
#undef  GGE_TARGET_PLATFORM
#define GGE_TARGET_PLATFORM         GGE_PLATFORM_LINUX
#endif


//////////////////////////////////////////////////////////////////////////
// post configure
//////////////////////////////////////////////////////////////////////////

// check user set platform
#if ! GGE_TARGET_PLATFORM
#error  "Cannot recognize the target platform; are you targeting an unsupported platform?"
#endif

#if (GGE_TARGET_PLATFORM == GGE_PLATFORM_WIN32)
#ifndef __MINGW32__
#pragma warning (disable:4127)
#endif
#endif  // GGE_PLATFORM_WIN32

#if ((GGE_TARGET_PLATFORM == GGE_PLATFORM_ANDROID) || (GGE_TARGET_PLATFORM == GGE_PLATFORM_IOS))
#define GGE_PLATFORM_MOBILE
#else
#define GGE_PLATFORM_PC
#endif

#endif // !_PLATFORMCONFIG_H
